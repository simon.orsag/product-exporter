<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductExportTask
 *
 * @property int $id
 * @property string $batch_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 */
class ProductExportTask extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'batch_id',
    ];
}
