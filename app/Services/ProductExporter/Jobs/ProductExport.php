<?php

namespace App\Services\ProductExporter\Jobs;

use App\Models\Product;
use App\Services\ProductExporter\Api\ExternalApi;
use App\Services\ProductExporter\Exceptions\ProductExportFailedException;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProductExport implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public int $tries = 3;

    /**
     * The number of seconds to wait before retrying the job.
     *
     * @var int
     */
    public int $backoff = 5;

    /**
     * @var int
     */
    private int $productId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $productId)
    {
        $this->productId = $productId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Product $model, ExternalApi $externalApi)
    {
        if ($this->batch()->cancelled()) {
            return;
        }

        $product = $model->query()->findOrFail($this->productId);
        $result = $externalApi->exportProduct($product);

        if (!$result) {
            $this->fail(ProductExportFailedException::class);
        }
    }
}
