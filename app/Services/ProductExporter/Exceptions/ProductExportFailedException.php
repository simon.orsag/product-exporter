<?php

namespace App\Services\ProductExporter\Exceptions;

class ProductExportFailedException extends \Exception
{
}
