<?php

namespace App\Services\ProductExporter;

use Illuminate\Support\Facades\Facade;

class ProductExporterFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'product.exporter';
    }
}
