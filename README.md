# Product Exporter

Ukázka možné iplementace exporteru produktů do externího REST API.

Projekt je napsaný v Laravel frameworku. Složky / soubory frameworku jsem odstranil, jedná se pouze o myšlenku, nikoliv o funkční/otestovaný kód.

# Myšlenka

- Produkty z DB postupně vybírám po "částech" - např. po 100.
- Pro každý produkt z části se vytvoří job ("task na pozadí"), vytvořené joby se přidají do batche ("seskupené joby").
- Batch se dispatche a zpracuje se na pozadí workerem (může jich být více).
- V každém jobu se zavolá REST API a produkt se exportuje
- Pokud export produktu selže, daný job se označí jako failed, je možné spustit export těchto produktů znovu, případně zjistit, které produkty se neexportovaly.
